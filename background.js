chrome.runtime.onInstalled.addListener(function (msg, sender, sendResponse) {
	chrome.action.disable();

	chrome.declarativeContent.onPageChanged.removeRules(undefined, function () {
		var rule1 = {
			conditions: [
				new chrome.declarativeContent.PageStateMatcher({
					pageUrl: { hostEquals: 'www.facebook.com' }
				})
			],
			actions: [new chrome.declarativeContent.ShowPageAction()]
		};
		let rules = [rule1];
		chrome.declarativeContent.onPageChanged.addRules(rules);
	});
});